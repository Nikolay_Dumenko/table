import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import { warning } from './utils';

const propTypes = {
  children: PropTypes.node,
  isAdaptive: PropTypes.bool,
  classNames: PropTypes.object,
  className: PropTypes.string,
  dataFormatted: PropTypes.func,
  style: PropTypes.object
};

const defaultProps = {
  className: '',
  style: {}
};

class TableFooter extends Component {

  renderRows() {
    const {
      type,
      isAdaptive,
      classNames,
      dataFormatted
    } = this.props;

    const numChildren = Children.count(this.props.children);

    if (numChildren < 1) return null;

    const props = {
      type,
      isAdaptive,
      dataFormatted,
      classNames
    };

    return Children.map(this.props.children, child => {
      const children = [];

      if (child.type.name !== 'TableRow') {
        warning(`<${child.type.name || child.type}> cannot appear as a child of <TableFooter>`);
      }

      Children.forEach(child.props.children, child => {
        children.push(child);
      });

      return React.cloneElement(child, props, children);
    });
  }

  render() {
    const { classNames, className, style } = this.props;

    const attr = {
      className: `${classNames.foot} ${className}`,
      style
    };

    return (
      <tfoot {...attr}>
        { this.renderRows() }
      </tfoot>
    );
  }
}

TableFooter.propTypes = propTypes;
TableFooter.defaultProps = defaultProps;

export default TableFooter;
