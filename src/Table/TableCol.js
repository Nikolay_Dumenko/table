import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  header: PropTypes.array,
  isAdaptive: PropTypes.bool,
  type: PropTypes.string,
  classNames: PropTypes.object,
  className: PropTypes.string,
  style: PropTypes.object,
  colspan: PropTypes.number,
  rowspan: PropTypes.number
};

const defaultProps = {
  className: '',
  style: {}
};

class Column extends Component {

  renderContentColumn() {
    const { isAdaptive, header, dataFormatted } = this.props;

    if (isAdaptive) {
      return [
        <span key={0}>
          { header }
        </span>,
        <span key={1}>
          { this.props.children }
        </span>
      ];
    }

    if (typeof dataFormatted === 'function') {
      return dataFormatted(this.props.children);
    }

    return this.props.children;
  }

  renderColumn() {
    const {
      type,
      style,
      colspan,
      rowspan,
      className,
      classNames,
    } = this.props;

    const attr = {
      className: `${classNames.col} ${className}`,
      style,
      colSpan: colspan,
      rowSpan: rowspan
    };

    if (type === 'head') {
      return (
        <th { ...attr }>
          { this.renderContentColumn() }
        </th>
      );
    }

    if (type === 'foot') {
      return (
        <td { ...attr } >
          { this.props.children }
        </td>
      )
    }

    return (
      <td { ...attr } >
        { this.renderContentColumn() }
      </td>
    );
  }

  render() {
    return this.renderColumn();
  }
}

Column.propTypes = propTypes;
Column.defaultProps = defaultProps;

export default Column;