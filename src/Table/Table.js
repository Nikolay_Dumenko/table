import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import { warning } from './utils';

const propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object,
  first: PropTypes.bool,
  adaptiveStart: PropTypes.number,
  dataFormatted: PropTypes.func
};

const defaultProps = {
  adaptiveStart: 660,
  className: '',
  first: false,
  style: {}
};

class Table extends Component {
  constructor(props) {
    super(props);

    this.state = {
      headers: [],
      isAdaptive: false,
      classNames: {
        table: '',
        title: '',
        head: '',
        body: '',
        foot: '',
        row: '',
        col: ''
      }
    };
  }

  componentDidMount() {
    let windowWidth = this.getWindowWidth();
    this.isAdaptive(windowWidth);

    window.addEventListener('resize', () => {
      windowWidth = this.getWindowWidth();
      this.isAdaptive(windowWidth);
    });
  }

  getWindowWidth() {
    return window.innerWidth;
  }

  isAdaptive(width) {
    const { adaptiveStart } = this.props;
    const isAdaptive = width <= adaptiveStart && adaptiveStart >= 320;

    this.setState({
      isAdaptive
    });

    this.setClass(isAdaptive);
  }

  setClass(isAdaptive) {
    const table = isAdaptive ? 'table-mob' : 'table-desk';
    const firstStyle = this.props.first ? `${table}__col--first` : '';

    this.setState({
      classNames: {
        table: `${table}`,
        title: `${table}__title`,
        head: `${table}__header`,
        body: `${table}__body`,
        foot: `${table}__foot`,
        row: `${table}__row`,
        col: `${table}__col ${firstStyle}`
      }
    });
  }

  handleAddHeader(header) {
    this.setState({
      headers: this.state.headers.push(header)
    });
  }

  // Renders

  renderTitle(base) {
    const { classNames, isAdaptive } = this.state;

    const props = {
      classNames,
      isAdaptive
    };

    return React.cloneElement(
      base,
      props
    );
  }

  renderHead(base) {
    const { dataFormatted } = this.props;
    const { classNames } = this.state;

    const type = 'head';
    const props = {
      type,
      addHeader: this.handleAddHeader.bind(this),
      dataFormatted,
      classNames
    };

    return React.cloneElement(
      base,
      props
    );
  }

  renderBody(base) {
    const { dataFormatted } = this.props;
    const {
      headers,
      isAdaptive,
      classNames
    } = this.state;


    const type = 'body';
    const props = {
      type,
      headers,
      isAdaptive,
      dataFormatted,
      classNames
    };

    return React.cloneElement(
      base,
      props
    );
  }

  renderFoot(base) {
    const { dataFormatted } = this.props;
    const {
      classNames,
      isAdaptive
    } = this.state;

    const type = 'foot';
    const props = {
      type,
      isAdaptive,
      dataFormatted,
      classNames
    };

    return React.cloneElement(
      base,
      props
    );
  }

  render() {
    const {
      className,
      style
    } = this.props;

    const { classNames } = this.state;

    let title = null;
    let tHead = null;
    let tBody = null;
    let tFoot = null;

    Children.forEach(this.props.children, (child) => {
      if (!React.isValidElement(child)) warning(`${child} is not valid`);

      const { name } = child.type;
      switch (name) {
        case 'TableTitle':
          title = this.renderTitle(child);
          break;
        case 'TableHeader':
          tHead = this.renderHead(child);
          break;
        case 'TableBody':
          tBody = this.renderBody(child);
          break;
        case 'TableFooter':
          tFoot = this.renderFoot(child);
          break;
        default:
          warning(`<${name || child.type}> cannot appear as a child of <Table>`);
      }
    });

    const attr = {
      className: `${classNames.table} ${className}`,
      style
    };

    return (
      <table {...attr}>
        { title }
        { tHead }
        { tBody }
        { tFoot }
      </table>
    );
  }
}

Table.propTypes = propTypes;
Table.defaultProps = defaultProps;

export default Table;