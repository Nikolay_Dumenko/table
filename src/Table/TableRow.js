import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  type: PropTypes.string,
  style: PropTypes.object,
  isAdaptive: PropTypes.bool,
  className: PropTypes.string,
  classNames: PropTypes.object,
  dataFormatted: PropTypes.func
};

const defaultProps = {
  className: '',
  style: {}
};

class TableRow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      headers: []
    };
  }

  componentDidMount() {
    this.setState({
      headers: this.props.headers
    });
  }

  createColumn() {
    const {
      type,
      isAdaptive,
      classNames,
      dataFormatted
    } = this.props;
    const { headers } = this.state;

    return Children.map(this.props.children, (child, index) => {
      const { children } = child.props;
      let props = {
        type,
        isAdaptive,
        classNames,
        dataFormatted
      };

      if (type === 'body') {
        props.header = headers[index];
      }

      return React.cloneElement(child, props, children);
    });
  }

  render() {
    const { classNames, className, style } = this.props;

    const attr = {
      className: `${classNames.row} ${className}`,
      style
    };

    return (
      <tr {...attr}>
        { this.createColumn() }
      </tr>
    )
  }
}

TableRow.propTypes = propTypes;
TableRow.defaultProps = defaultProps;

export default TableRow;