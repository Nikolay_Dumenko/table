import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import { warning } from './utils';

const propTypes = {
  children: PropTypes.node,
  isAdaptive: PropTypes.bool,
  classNames: PropTypes.object,
  className: PropTypes.string,
  dataFormatted: PropTypes.func,
  style: PropTypes.object
};

const defaultProps = {
  className: '',
  style: {}
};

class TableBody extends Component {

  createRows() {
    const {
      headers,
      type,
      isAdaptive,
      dataFormatted,
      classNames
    } = this.props;
    const numChildren = Children.count(this.props.children);

    if (numChildren < 1) return null;

    const props = {
      type,
      headers,
      isAdaptive,
      dataFormatted,
      classNames
    };

    return Children.map(this.props.children, child => {
      const children = [];

      if (child.type.name !== 'TableRow') {
        warning(`<${child.type.name || child.type}> cannot appear as a child of <TableBody>`);
      }

      Children.forEach(child.props.children, child => {
        children.push(child);
      });

      return React.cloneElement(child, props, children);
    });
  }

  render() {
    const { classNames, className, style } = this.props;

    const attr = {
      className: `${classNames.body} ${className}`,
      style
    };

    return (
      <tbody {...attr}>
      { this.createRows() }
      </tbody>
    );
  }
}

TableBody.propTypes = propTypes;
TableBody.defaultProps = defaultProps;

export default TableBody;