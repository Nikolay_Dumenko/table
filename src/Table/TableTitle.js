import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.node,
  classNames: PropTypes.object,
  isAdaptive: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object
};

const defaultProps = {
  className: '',
  style: {}
};

class TableTitle extends Component {
  render() {
    const {
      children,
      classNames,
      className,
      style
    } = this.props;

    const attr = {
      className: `${classNames.title} ${className}`,
      style
    };

    return (
      <caption { ...attr }> { children } </caption>
    );
  }
}

TableTitle.propTypes = propTypes;
TableTitle.defaultProps = defaultProps;

export default TableTitle;