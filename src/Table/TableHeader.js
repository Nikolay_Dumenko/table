import React, { Component, Children } from 'react';
import PropTypes from 'prop-types';
import { warning } from './utils';

const propTypes = {
  children: PropTypes.node,
  addHeader: PropTypes.func,
  classNames: PropTypes.object,
  className: PropTypes.string,
  dataFormatted: PropTypes.func,
  style: PropTypes.object
};

const defaultProps = {
  className: '',
  style: {}
};

class TableHeader extends Component {
  componentDidMount() {
    this.getHeaders();
  }

  getHeaders() {
    const { addHeader } = this.props;
    const childrenArray = Children.toArray(this.props.children);
    const childrenNum = childrenArray.length;
    const child = childrenArray[0];

    if (childrenNum  < 1) return null;

    Children.map(child, child => {
      Children.forEach(child.props.children, child => {
        addHeader(child.props.children);
      });
    });
  }

  createRows() {
    const {
      type,
      classNames,
      dataFormatted
    } = this.props;

    const childrenArray = Children.toArray(this.props.children);
    const childrenNum = childrenArray.length;
    const child = childrenArray[0];

    if (childrenNum  < 1) return null;
    if (childrenNum > 1) warning('In the header there can be only one row');

    const children = [];
    const props = {
      type,
      classNames,
      dataFormatted
    };

    Children.forEach(child.props.children, child => {
      children.push(child);
    });

    return React.cloneElement(child, props, children);
  }

  render() {
    const { classNames, className, style } = this.props;

    const attr = {
      className: `${classNames.head} ${className}`,
      style
    };

    return (
      <thead { ...attr }>
        { this.createRows() }
      </thead>
    )
  }
}

TableHeader.propTypes = propTypes;
TableHeader.defaultProps = defaultProps;

export default TableHeader;