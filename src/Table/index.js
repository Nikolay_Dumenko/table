import Table from './Table';
import TableTitle from './TableTitle';
import TableHeader from './TableHeader';
import TableBody from './TableBody';
import TableFooter from './TableFooter';
import TableRow from './TableRow';
import TableCol from './TableCol';

import './style.css';

export {
  TableTitle,
  TableHeader,
  TableBody,
  TableFooter,
  TableRow,
  TableCol
};

export default Table;