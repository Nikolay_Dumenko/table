import React from 'react';
import Table, {
  TableTitle,
  TableHeader,
  TableBody,
  TableFooter,
  TableRow,
  TableCol
} from './Table';

const headers = [' ', 'Названия', 'Автор', 'Год', 'Цена' ];
const data = [
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  },
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  },
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  },
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  },
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  },
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  },
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  },
  {
    name: 'Алгоритмы. Справочник с примерами на C, C++, Java и Python',
    author: 'Джордж Хайнеман',
    year: 2017,
    price: 450
  },
  {
    name: 'Искусственный интеллект и принятие решений. 2011-Вып.1',
    author: 'Емельянова С.В.',
    year: 2011,
    price: 292
  },
  {
    name: 'Оптимизация программ на C++. Проверенные методы повышения производительности',
    author: 'Курт Гантерот',
    year: 2017,
    price: 590
  }
];

function dataFormatted(children) {
  switch (typeof children) {
    case 'string':
      return (
        <p className="table-col--left"> { children } </p>
      );
    case 'number':
      return (
        <p className="table-col--right"> { children } </p>
      );
    case 'object':
      return (
        <p className="table-col--center"> { children } </p>
      );
    default:
      return children;
  }
}

const App = () => (
  <div>
    <Table
      adaptiveStart={800}
      dataFormatted={dataFormatted}
      first
    >
      <TableTitle> Title </TableTitle>
      <TableHeader>
        <TableRow>
          {
            headers.map((item, index) => (
              <TableCol key={index}> { item } </TableCol>
            ))
          }
        </TableRow>
      </TableHeader>

      <TableBody>

        {
          data.map((item, index) => (
            <TableRow key={index}>
              <TableCol>
                <input type="checkbox"/>
              </TableCol>
              <TableCol>
                {item.name}
              </TableCol>
              <TableCol>
                {item.author}
              </TableCol>
              <TableCol>
                {item.year}
              </TableCol>
              <TableCol>
                {item.price}
              </TableCol>
            </TableRow>
          ))
        }

      </TableBody>

      <TableFooter>
        <TableRow>
          <TableCol colspan={4}>
            <span>
              price:
            </span>
            <span>
              {
                data.reduce((previousValue, currentValue) => (
                  previousValue + currentValue.price
                ), 0)
              }
            </span>
          </TableCol>
        </TableRow>
      </TableFooter>
    </Table>
  </div>
);

export default App;
