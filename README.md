# **React Table** #


**Example**

```
#!jsx
<Table 
  className='table' 
  adaptiveStart={600}
  first
>
  <Title> Title </Title>
  <TableHeader>
    <TableRow>
      <TableCol>
        Header text
      </TableCol>
      <TableCol>
        Header text
      </TableCol>
      <TableCol>
        Header text
      </TableCol>
    </TableRow>
  </TableHeader>
  <TableBody>
    <TableRow>
      <TableCol>
        Body text
      </TableCol>
      <TableCol>
        Body text
      </TableCol>
      <TableCol>
        Body text
      </TableCol>
    </TableRow>
    <TableRow>
      <TableCol>
        Body text
      </TableCol>
      <TableCol>
        Body text
      </TableCol>
      <TableCol>
        Body text
      </TableCol>
    </TableRow>
  </TableBody>
  <TableFooter>
     <TableRow>
      <TableCol colspan={3}>
        Footer text
      </TableCol>
    </TableRow>
  </TableFooter>
</Table>
```

**Table attribute**

* children: node
* className: string
* style: object
* adaptiveStart: number
* first: bool

**TableTitle attribute**

* className: string
* style: object


**TableHeader attribute**

* className: string
* style: object

**TableBody attribute**

* className: string
* style: object

**TableFooter attribute**

* className: string
* style: object

**TableRow attribute**

* className: string
* style: object

**TableCol attribute**

* className: string
* style: object
* colspan: number
* rowspan: number